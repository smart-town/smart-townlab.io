---
title: "关于记笔记"
date: 2022-01-04T21:33:45+08:00
draft: false
categories: ['daily']
---

唔，今天干了什么呢？

本来想要看看 jest 和 ts（ js 的单元测试），但是摸摸鱼？

摸了什么鱼嘞，发现了**卡片笔记法**，还有几个新的笔记工具，心里琢磨着要不要试一试？

在哪里发现的嘞？一个 RSS 订阅工具：inoreader. 访问需要科学上网！

中午吃的什么饭嘞？有肉！

下午琢磨研究了一个 shell 脚本，长这样儿：

```shell
host="https://apis.map.qq.com/ws"

if test "$TC_DT_AK" = ""
then
    echo "Not set ak"
    exit 1
fi
#####################################
function from_ip() {
    curl -s "${host}/location/v1/ip?ip=$1&key=$TC_DT_AK"
}
function from_longlati() {
    curl -s "${host}/geocoder/v1?location=$1,$2&key=$TC_DT_AK"
}
#####################################
if test -n "$1"
then
    echo "Get Addr from IP: $@"
    data=$(from_ip $1)
    #echo "data: $data\n"
    param=`echo $data | jq '.result.location|.lat,.lng' | xargs`
    echo "lat,lng: $param"
    from_longlati $param | jq '.result.address'
    echo "--------"
else
    echo "SYNTAX: order ip"
fi
```

设置一个`alias`，结合`tail | cut | xargs file.sh`可以快速获取到 ip 对应的位置。哦对了，还有一个 linux 命令行工具：`jq`，专门用来处理 json。最后最后，再把这个快捷命令放到`~/.bash_profile`里面，一登录就能看到我想看到 👀。

哦对了，以后大概一定要养成刷题的习惯了。嗯。没事瞅两眼。

热爱生活。